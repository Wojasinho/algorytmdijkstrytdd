# Uruchomienie algorytmu w celu wykorzystania funkcji w niej zaimplementowanych
include("AlgorytmDijkstry_FifthTDD.jl")

# Załaczenie bibliotek, z ktorych bedziemy korzystac
using FactCheck



# Test 5
# Test który sprawdzi czy algorytm poprawnie znajdzie dlugosc najkrotszej sciezki,
# oraz punkty po ktorych przechodzil dla zadanego grafu


facts("Najkrotsza droga bedzie rowna 13 z punktu A do B na podstawie poniszego grafu") do
	graf =Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
	"b"=> (Dict("c"=>9,"f"=>6) ),
	"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
	"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
	"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
	"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));


	wynik = main(graf)

	@fact wynik[1] --> 11
	@fact wynik[2] --> ["b","c","e"]

end
println("Wszystkie testy zaliczone")
