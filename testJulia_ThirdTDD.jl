
# Uruchomienie algorytmu w celu wykorzystania funkcji w niej zaimplementowanych
include("AlgorytmDijkstry_ThirdTDD.jl")

# Załaczenie bibliotek, z ktorych bedziemy korzystac
using FactCheck

# Test 3
# Funkcja czyStartRownyKoniec() sprawdzi czy poczatkowy punkt jest rowny koncowemu. Jesli tak to zwroci true

facts("Zwroci true jesli punkt start bedzie rowny punktowi koniec") do
	@fact czyStartRownyKoniec("a","a") --> true
	@fact czyStartRownyKoniec("b", "a") --> false
end
println("Wszystkie testy zaliczone")
