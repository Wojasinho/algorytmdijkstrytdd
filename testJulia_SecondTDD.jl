# Uruchomienie algorytmu w celu wykorzystania funkcji w niej zaimplementowanych
include("AlgorytmDijkstry_SecondTDD.jl")

# Załaczenie bibliotek, z ktorych bedziemy korzystac
using FactCheck
#
# Test 2
# Funkcja sprawdzPunkty ktora jest czescia algorytmDijkstry jako pierwszy i drugi argument przyjmuje punkt poczatkowy
# oraz punkt startowy. Jesli dlugosc punktow jest wieksza od jeden oznacza to bledny
# format danych wejsciowych
# Dane wejsciowe :

graf =Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
"b"=> (Dict("c"=>9,"f"=>6) ),
"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));

facts("Zwroci true jesli warunki argumenty funkcji beda sie zgadzac") do
	@fact sprawdzPunkty("a","b") --> true
	@fact sprawdzPunkty("aa","b") --> false
end
	println("Wszystkie testy zaliczone")
