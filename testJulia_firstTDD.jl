# Uruchomienie algorytmu w celu wykorzystania funkcji w niej zaimplementowanych
include("AlgorytmDijkstry_FirstTDD.jl")

# Załaczenie bibliotek, z ktorych bedziemy korzystac
using FactCheck


# Test 1
# Funkcja algorytmDijkstry przyjmuje jako argument slownik
# W przeciwnym wypadku, jesli podamy inna wartosc, otrzymamy blad
facts("Sprawdz poprawnosc argumentu pierwszego") do

	@fact main(1000) --> Dict()
	@fact main("String jako input") --> Dict()

  @fact main([(1.10,10000,2000)]) -->
  			Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
			"b"=> (Dict("c"=>9,"f"=>6) ),
			"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
			"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
			"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
			"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ))
end
println("Wszystkie testy zaliczone")
