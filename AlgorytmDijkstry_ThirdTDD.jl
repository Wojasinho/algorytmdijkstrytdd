function algorytmDijkstry!(graf::Dict{String,Dict{String,Int64}},start,koniec,odwiedzone, dystanse, przodkowie)
  if (sprawdzPunkty(start,koniec)==true)
      if (czyStartRownyKoniec(start,koniec)==true)
          sciezka=[]
                  while (koniec != nothing)
                      push!(sciezka,koniec)
                      koniec=get(przodkowie,koniec,nothing)
                  end
                  print(dystanse[start])
                  return print(reverse(sciezka))
      end

    if isempty(odwiedzone)
        dystanse[start]=0
    end

    for sasiad in keys(graf[start])
        if !(sasiad in odwiedzone)
            # print("Test2")
            odlegloscSasiada=get(dystanse,sasiad,99999)
            testOdleglosc = dystanse[start]+graf[start][sasiad]
            if (testOdleglosc < odlegloscSasiada)

                # push!(dystanse,(sasiad=>odlegloscSasiada))
                dystanse[sasiad]=testOdleglosc;
                # push!(przodkowie,(sasiad=>start))
                przodkowie[sasiad]=start;
            end
        end
    end
    push!(odwiedzone,start)
    clear!(:nieOdwiedzone)
    nieOdwiedzone = Dict{}();

    for i in keys(graf)
        if !(i in odwiedzone)
            wartosc = get(dystanse,i,99999);
            push!(nieOdwiedzone,(i=>wartosc))
            # nieOdwiedzone[i]=get(dystanse,i,99999);
        end

    end
    najblizszyWezel=collect(keys(nieOdwiedzone))[indmin(collect(values(nieOdwiedzone)))]

    # return algorytmDijkstry(graf,najblizszyWezel,koniec,odwiedzone,dystanse,przodkowie)

    return (algorytmDijkstry!(graf,najblizszyWezel,koniec,odwiedzone,dystanse,przodkowie))


else
    return false
end
end

function czyStartRownyKoniec(start,koniec)
    if start == koniec
        return true

else
    return false
end
end


function sprawdzPunkty(start, koniec)
    if sizeof(start) <= 1 && sizeof(koniec) <= 1
        return true
    else
        return false
    end
end


graf = Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
     "b"=> (Dict("c"=>9,"f"=>6) ),
     "c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
     "d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
     "e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
     "f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));

function main()
  try
    start="a"
    koniec="b"
    odwiedzone = []
    dystanse=Dict{}()
    przodkowie=Dict{}()
    algorytmDijkstry!(graf,start,koniec,odwiedzone, dystanse, przodkowie)

  catch e
    if isa(e, MethodError)
      return print("Prosze podac jako argument funkcji, zagniezdzony Slownik z punktami,
      oraz punkt poczatkowy i koncowy (jednoelementowy)")
    else
      return e
    end
  end
end

   # return algorytmDijkstry(graf,najblizszyWezel,koniec,odwiedzone,dystanse,przodkowie)
# print(main())
