# Uruchomienie algorytmu w celu wykorzystania funkcji w niej zaimplementowanych
include("AlgorytmDijkstry.jl")

# Załaczenie bibliotek, z ktorych bedziemy korzystac
using FactCheck


# Test 1
# Funkcja algorytmDijkstry przyjmuje jako argument slownik
# W przeciwnym wypadku, jesli podamy inna wartosc, otrzymamy blad
# facts("Sprawdz poprawnosc argumentu pierwszego") do
#
# 	@fact main(1000) --> Dict()
# 	@fact main("String jako input") --> Dict()
# 	@fact main([(1.23, 5.55)]) --> "Please provide array of tuples where each element is Int64"
#
#   @fact main([(1.10,10000,2000)]) -->
#   			Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
# 			"b"=> (Dict("c"=>9,"f"=>6) ),
# 			"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
# 			"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
# 			"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
# 			"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ))
# end
# println("Wszystkie testy zaliczone")

# Test 2
# Funkcja sprawdzPunkty ktora jest czescia algorytmDijkstry jako pierwszy i drugi argument przyjmuje punkt poczatkowy
# oraz punkt startowy. Jesli dlugosc punktow jest wieksza od jeden oznacza to bledny
# format danych wejsciowych
# Dane wejsciowe :

# graf =Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
# "b"=> (Dict("c"=>9,"f"=>6) ),
# "c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
# "d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
# "e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
# "f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));
#
# facts("Zwroci true jesli warunki argumenty funkcji beda sie zgadzac") do
# 	@fact sprawdzPunkty("a","b") --> true
# 	@fact sprawdzPunkty("aa","b") --> false
# end
# 	println("Wszystkie testy zaliczone")

# Test 3
# Funkcja czyStartRownyKoniec() sprawdzi czy poczatkowy punkt jest rowny koncowemu. Jesli tak to zwroci true

# facts("Zwroci true jesli dany punkt poczatkowy bedzie rowny koncowemu") do
# 	@fact czyStartRownyKoniec("a","a") --> true
# 	# @fact czyStartRownyKoniec("b", "a") -->
# end
# println("Wszystkie testy zaliczone")

# Test 4
# Test który sprawdzi czy algorytm poprawnie znajdzie sciezke dla zadanego grafu (po refaktoryzacji)
#
# facts("Najkrotsza droga bedzie rowna 13 z punktu A do B na podstawie poniszego grafu") do
# 	graf =Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
# 	"b"=> (Dict("c"=>9,"f"=>6) ),
# 	"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
# 	"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
# 	"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
# 	"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));
#
# 	odwiedzone = []
#     dystanse=Dict{}()
#     przodkowie=Dict{}()
# 	wynik = algorytmDijkstry!(graf,"a","b",odwiedzone, dystanse, przodkowie)
#
# 	@fact wynik[1]--> 13
#
# end
# println("Wszystkie testy zaliczone")


# Test 5
# Test który sprawdzi czy algorytm poprawnie znajdzie dlugosc najkrotszej sciezki,
# oraz punkty po ktorych przechodzil dla zadanego grafu


facts("Najkrotsza droga bedzie rowna 13 z punktu A do B na podstawie poniszego grafu") do
	graf =Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
	"b"=> (Dict("c"=>9,"f"=>6) ),
	"c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
	"d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
	"e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
	"f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));


	wynik = main(graf)

	@fact wynik[1] --> 11
	@fact wynik[2] --> ["b","c","e"]

end
println("Wszystkie testy zaliczone")
