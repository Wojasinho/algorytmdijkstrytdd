## Aplikacja do na rzecz projektu realizowanego w ramach przedmiotu
# Testowanie i weryfkacjia oprogramowania
# Politechnika Warszawska
# Wykonał Wojciech Koszela
# Nr indeksu : 295376
# Prowadzący : doc. dr inż. Wojciech Koziński

# Definicja grafu
graf = Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
     "b"=> (Dict("c"=>9,"f"=>6) ),
     "c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
     "d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
     "e"=> (Dict("a"=>9,"c"=>2, "d"=>10, "f"=>11) ),
     "f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));
# Główna funkcja która realizuje algorytm Dijkstry

function algorytmDijkstry!(graf::Dict{String,Dict{String,Int64}},start,koniec,odwiedzone, dystanse, przodkowie)
  if (sprawdzPunkty(start,koniec)==true)
      if (czyStartRownyKoniec(start,koniec)==true)
          sciezka=[]
                  while (koniec != nothing)
                      push!(sciezka,koniec)
                      koniec=get(przodkowie,koniec,nothing)
                  end

                  rezultat = []
                  push!(rezultat,dystanse[start])
                  push!(rezultat,reverse(sciezka))
                  return rezultat

      end

    if isempty(odwiedzone)
        dystanse[start]=0
    end

    for sasiad in keys(graf[start])
        if !(sasiad in odwiedzone)
            # print("Test2")
            odlegloscSasiada=get(dystanse,sasiad,99999)
            testOdleglosc = dystanse[start]+graf[start][sasiad]
            if (testOdleglosc < odlegloscSasiada)

                # push!(dystanse,(sasiad=>odlegloscSasiada))
                dystanse[sasiad]=testOdleglosc;
                # push!(przodkowie,(sasiad=>start))
                przodkowie[sasiad]=start;
            end
        end
    end
    push!(odwiedzone,start)
    clear!(:nieOdwiedzone)
    nieOdwiedzone = Dict{}();

    nieOdwiedzone= aktualizujNieodwiedzone(graf, odwiedzone, nieOdwiedzone,dystanse)
    najblizszyWezel=collect(keys(nieOdwiedzone))[indmin(collect(values(nieOdwiedzone)))]


    return (algorytmDijkstry!(graf,najblizszyWezel,koniec,odwiedzone,dystanse,przodkowie))


else
    return false
end
end
# Funkcja pomocnicza, sprawdzajaca czy start jest rowny koncowi
function czyStartRownyKoniec(start,koniec)
    if start == koniec
        return true
    end
end

# Funkcja pomocnicza sprawdzjąca czy punkt startowy i koncowy jest mniejszy badz rowny 1
function sprawdzPunkty(start, koniec)
    if sizeof(start) <= 1 && sizeof(koniec) <= 1
        return true
    else
        return false
    end
end

# Aktualizacja nieOdwiedzonych punktów w danej iteracji
function aktualizujNieodwiedzone(graf, odwiedzone, nieOdwiedzone,dystanse)
    for i in keys(graf)
        if !(i in odwiedzone)
            wartosc = get(dystanse,i,99999);
            push!(nieOdwiedzone,(i=>wartosc))
            # nieOdwiedzone[i]=get(dystanse,i,99999);
        end
    end
    return nieOdwiedzone

end

function main()

    #Definicja grafów
    grafPierwszy = Dict("a"=> (Dict("c"=>14,"d"=>7, "e"=>2) ),
         "b"=> (Dict("c"=>9,"f"=>6) ),
         "c"=> (Dict("a"=>14,"b"=>9, "e"=>2) ),
         "d"=> (Dict("a"=>7,"e"=>10, "f"=>15) ),
         "e"=> (Dict("a"=>2,"c"=>2, "d"=>10, "f"=>11) ),
         "f"=> (Dict("b"=>6,"d"=>15, "e"=>11) ));

    grafDrugi = Dict("a"=> (Dict("c"=>11,"d"=>2, "e"=>10) ),
         "b"=> (Dict("c"=>9,"f"=>6) ),
         "c"=> (Dict("a"=>11,"b"=>9, "e"=>2) ),
         "d"=> (Dict("a"=>2,"e"=>10, "f"=>1) ),
         "e"=> (Dict("a"=>10,"c"=>2, "d"=>10, "f"=>21) ),
         "f"=> (Dict("b"=>6,"d"=>1, "e"=>21) ));

    grafTrzeci = Dict("a"=> (Dict("c"=>2,"e"=>3, "d"=>4) ),
         "b"=> (Dict("f"=>2) ),
         "c"=> (Dict("a"=>2,"f"=>9, "e"=>2) ),
         "d"=> (Dict("f"=>15),"a"=>4 ),
         "e"=> (Dict("a"=>3,"c"=>2, "f"=>5) ),
         "f"=> (Dict("b"=>2,"d"=>15, "e"=>5),"c"=>2 ));

         graf=[grafPierwszy,grafDrugi,grafTrzeci]
         # Początkowe intro
         print(
         "\n
         Witam, uruchomiłeś właśnie Aplikację za pomoca której, jesteś
         wstanie międzyinnymi wyznaczć najkrótszą drogę wybranego grafu za pomoca Algorytmu
         Dijkstry.

         Algorytm Dijkstry, opracowany przez holenderskiego informatyka
         Edsgera Dijkstre, służy do znajdowania najkrótszej ścieżki z pojedynczego źródła w grafie o
         nieujemnych wagach krawedzi.\n")

         print("
         Mając dany graf z wyróżnionym wierzchołkiem (źródłem) algorytm znajduje
         odległości od źródła do wszystkich pozostałych wierzchołków. Łatwo
         zmodyfikować go tak, aby szukał wyłącznie (najkrótszej) ścieżki do
         jednego ustalonego wierzchołka, po prostu przerywając działanie w
         momencie dojścia do wierzchołka docelowego, bądź transponując
         tablicę incydencji grafu.

         Algorytm Dijkstry znajduje w grafie wszystkie najkrótsze ścieżki pomiędzy
         wybranym wierzchołkiem a wszystkimi pozostałymi, przy okazji wyliczając
         również koszt przejścia każdej z tych ścieżek. \n \n ")

         print("W Aplikacji dostępne są 4 Funkcjonalności : \n")

         print("Funkcjonalność 1: Z dostepnych grafów wybierz jeden, i
         znajdź najkrótszą drogę pomiędzy zadanymi punktami \n")

         print("Funkcjonalność 2: Z dostepnych grafów wybierz jeden, i
         znajdź wszystkie najkrótsze drogi do wszystkich możliwych punktów\n")

         print("Funkcjonalność 3: Wykonaj testy zgodne z TDD z pierwszego Etapu
         projektu + podsumowanie \n")

         print("Funkcjonalność 4: Zakończ aplikację \n\n ")

         # Pobranie danych od użytkownika aplikacji
         print("Wybierz proszę jedną funkcjonalność [1-4] \n")
         numerZapytania=zapytanieUzytkownika()
         numerZapytania = parse(Int, chomp(numerZapytania))

         if(numerZapytania==1)

             print("Wybierz prosze jeden graf  dostepny w aplikacji[np. 1-3]\n")
             numerGrafu=zapytanieUzytkownika()

             # Parsowanie na Int
             numerGrafu = parse(Int, chomp(numerGrafu))

             print("Wybierz prosze punkt początkowy grafu [np.\"a\",\"b\",\"c\"]'\n")
             start=zapytanieUzytkownika()

             print("Wybierz prosze punkt koncowy grafu [np.\"a\",\"b\",\"c\"]'\n")
             # print("Wybierz prosze punkt koncowy grafu  [np.'a','b','c']'\n")

             koniec=zapytanieUzytkownika()

             # Stripowoanie poniewaz, po wprowadzeniu w konsoli "x", pojawia sie w pamieci
             # jako "\"x\""
             start = strip(start,['\"'])
             koniec= strip(koniec,['\"'])

             # Potrzebne zmienne do startu algorytmu
             odwiedzone = []
             dystanse=Dict{}()
             przodkowie=Dict{}()
             # startAlgorytmu(graf[numerGrafu],start,koniec,odwiedzone,dystanse,przodkowie)
             rezultat = startAlgorytmu(graf[numerGrafu],start,koniec,odwiedzone,dystanse,przodkowie)
             print("\n")
             print("Dlugosc nakrotszej scieżki z punktu: ")
             print(start)
             print(" do punktu: " )
             print(koniec)
             print(" wynosi: ")
             print(rezultat[1])
        end

        if (numerZapytania==2)
            print("Wybierz prosze jeden graf dostepny w aplikacji[np. 1-3]\n")
            numerGrafu=zapytanieUzytkownika()
            numerGrafu = parse(Int, chomp(numerGrafu))

            print("Wybierz prosze punkt początkowy grafu [np.\"a\",\"b\",\"c\"]'\n")
            start=zapytanieUzytkownika()
            start = strip(start,['\"'])
            odwiedzone = []
            dystanse=Dict{}()
            przodkowie=Dict{}()

            for koniec in keys(graf[numerGrafu])
                odwiedzone = []
                dystanse=Dict{}()
                przodkowie=Dict{}()

                if (start == koniec)
                    continue
                end
                # wywolanie funkcji i przygotowanie tekstu koncowego
                rezultat = startAlgorytmu(graf[numerGrafu],start,koniec,odwiedzone,dystanse,przodkowie)
                print("\n")
                print("Dlugosc nakrotszej scieżki z punktu: ")
                print(start)
                print(" do punktu: " )
                print(koniec)
                print(" wynosi: ")
                print(rezultat[1])


            end
        end

        if (numerZapytania==3)
            print("Pierwszy TDD [1] : Test sprawdzajacy poprawnosc argumentu pierwszego [Failed]\n")
            print("Drugi TDD [2] : Test sprawdzajacy poprawnosc podanych punktów (poczatkowy i końcowy). Test zwróci true jeśli warunki argumenty funkcji beda sie zgadzac\n")
            print("Trzeci TDD [3] : Test sprawdzajacy czy punkt początkowy równa się końcowemu. Test zwróci true jeśli dany punkt początkowu bedzie rowny końcowemu\n")
            print("Pierwszy TDD [4] : Test sprawdzający czy algorytm poprawnie znajdzie ścieżke dla zadanego grafu (po refaktoryzacji) \n")
            print("Pierwszy TDD [5] :Test sprawdzający czy algorytm poprawnie znajdzie dlugosc najkrotszej sciezki, oraz punkty po których przechodził dla zadanego grafu. Graf zdefiniowany przez autora testy\n")

            print("Wybierz prosze jeden z pieciu dostepnych testow w aplikacji[np. 1-5]\n")
            numerTestu=zapytanieUzytkownika()
            numerTestu = parse(Int, chomp(numerTestu))

            if(numerTestu==1)
                include("AlgorytmDijkstry_FirstTDD.jl")
                include("testJulia_FirstTDD.jl")
            end
            if(numerTestu==2)
                include("AlgorytmDijkstry_SecondTDD.jl")
                include("testJulia_SecondTDD.jl")
            end
            if(numerTestu==3)
                include("AlgorytmDijkstry_ThirdTDD.jl")
                include("testJulia_ThirdTDD.jl")
            end
            if(numerTestu==4)
                include("AlgorytmDijkstry_FourthTDD.jl")
                include("testJulia_FourthTDD.jl")
            end
            if(numerTestu==5)
                include("AlgorytmDijkstry_FifthTDD.jl")
                include("testJulia_FifthTDD.jl")
            end

    end

        if (numerZapytania == 4 )
            i=1
            while true
                if i >= 1
                    break
                end
                i += 1
            end

        end

    end
# Funkcja uruchamiająca algorytm
function startAlgorytmu(graf,start,koniec,odwiedzone,dystanse,przodkowie)

  try
      return(algorytmDijkstry!(graf,start,koniec,odwiedzone, dystanse, przodkowie))

  catch e
    if isa(e, MethodError)
      return print("Prosze podac jako argument funkcji, zagniezdzony Slownik z punktami,
      oraz punkt poczatkowy i koncowy (jednoelementowy)")
    else
      return e
    end
  end
end

# Funkcja do przechywytywania danych wejściowych z klawiatury
function zapytanieUzytkownika()
return readline()
end

print(main())
